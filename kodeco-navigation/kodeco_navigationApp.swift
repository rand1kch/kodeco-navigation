//
//  kodeco_navigationApp.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 16.12.2022.
//

import SwiftUI

@main
struct kodeco_navigationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
