//
//  DetailView.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 16.12.2022.
//

import SwiftUI
import MapKit

struct DetailView: View {
    
    let user: User
    @State private var showMap = false
    @State var region = MKCoordinateRegion()
    
    var body: some View {
        VStack{
            Text("Username: " + user.name)
                .font(.title)
                .padding(.bottom)
            Text(user.description)
                .padding(.horizontal)
            Spacer()
            Button(action: {
                showMap = true
            }){
                HStack{
                    Image(systemName: "mappin.and.ellipse")
                    Text(user.location)
                        .font(.headline)
                }
            }.padding()
                .fullScreenCover(isPresented: $showMap){
                    VStack{
                        HStack{
                            Text("You open bottom sheet!")
                            Spacer()
                            Button("Done"){
                                showMap = false
                            }
                        }.padding(.horizontal)
                        Map(coordinateRegion: $region)
                    }
                }
        }
    }
}

struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(user: User(name: "Den", description: "fsdfsdfdsfdsfdsfsfsddsfdsfsdfdsfdsfdsfdsfdsffsfsdf", location: "Location"))
    }
}
