//
//  User.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 16.12.2022.
//

import Foundation

struct User {
    let id = UUID()
    let name: String
    let description: String
    let location: String
}

extension User: Identifiable { }

let users = [
    User(name: "Vasya", description: "01fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya1", description: "2fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya2", description: "3fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya3", description: "4fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya4", description: "5fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya5", description: "6fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya6", description: "7fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya7", description: "8fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
    User(name: "Vasya8", description: "9fdsjflsdfjdslfsdkfjsflksfjlskdjdsfjksldfjksdf", location: "Улица Пушкина, дом Колотушкина"),
]
