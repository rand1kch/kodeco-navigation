//
//  MoveGestureAnimation.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 17.12.2022.
//

import SwiftUI

struct MoveGestureAnimation: View {
    
    @State private var dragAmount = CGSize.zero
    
    var body: some View {
        Circle()
            .frame(width: 100, height: 100, alignment: .center)
            .foregroundColor(.yellow)
            .offset(dragAmount)
            .animation(.default, value: dragAmount)
            .gesture(
                DragGesture()
                    .onChanged { action in dragAmount = action.translation }
                    .onEnded {_ in
                        withAnimation(.spring()){
                            dragAmount = CGSize.zero
                        }
                    }
            )
            .padding()
    }
}

struct MoveGestureAnimation_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            MoveGestureAnimation()
        }
    }
}
