//
//  MultipleAnimationEnum.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 21.12.2022.
//

import Foundation

enum MultipleAnimationEnum {
    case none
    case opened
    case moved
}
