//
//  RotationAnimation.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 17.12.2022.
//

import SwiftUI

struct RotationAnimation: View {
    
    @State private var isSquare = false
    @State private var isRotation = 0.0
    
    var body: some View {
        VStack{
            if(isSquare) {
                Rectangle()
                    .frame(width: 100, height: 100, alignment: .center)
                    .rotationEffect(.degrees(isRotation))
                    .onAppear{
                        withAnimation(.linear(duration: 0.3)){
                            isRotation = 90.0
                        }
                    }
                    .onTapGesture {
                        withAnimation(.linear(duration: 0.3)){
                            isRotation = 0.0
                        }
                        isSquare.toggle()
                    }
            } else {
                Circle().frame(width: 100, height: 100, alignment: .center)
                    .rotationEffect(.degrees(isRotation))
                    .onTapGesture {
                        isSquare.toggle()
                    }
                
            }
        }.animation(.default, value: isSquare)
            .padding()
            .foregroundColor(.cyan)
    }
}

struct RotationAnimation_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            RotationAnimation()
        }
    }
}
