//
//  AnimationView.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 17.12.2022.
//

import SwiftUI

struct AnimationView: View {

    
    var body: some View {
        VStack{
            ChangeColorAnimation()
            MoveGestureAnimation()
            RotationAnimation()
            MultipleAnimationView().padding()
            Spacer()
        }
    }
}

struct AnimationView_Previews: PreviewProvider {
    static var previews: some View {
        AnimationView()
    }
}
