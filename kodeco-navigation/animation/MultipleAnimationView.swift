//
//  MultipleAnimationView.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 21.12.2022.
//

import SwiftUI

struct MultipleAnimationView: View {
    
    private let circleCount = 12
    private let squeezeOffset = 20
    private let defaultOffset = 30
    
    @State private var isOffseted = false
    @State private var rotationStep = 0
    @State private var isAnimated = false
    
    var body: some View {
        VStack{
            ZStack{
                ForEach(0..<circleCount){ index in
                    SpinnerItem(
                        rotation: .init(index + rotationStep) / .init(circleCount) * 360,
                        isOffseted: isOffseted,
                        offset: CGFloat(isOffseted ? squeezeOffset : defaultOffset),
                        isAnimated: isAnimated
                    )
                }
            }.onTapGesture {
                isAnimated = true
                animate()
            }
        }.frame(width: 100, height: 100, alignment: .center)
    }
    
    func animate(){
        Timer.scheduledTimer(withTimeInterval: 1.2, repeats: true){ timer in
            isOffseted.toggle()
            rotationStep = rotationStep + 4
            if(rotationStep == 20) {
                timer.invalidate()
                withAnimation{
                    isAnimated = false
                    isOffseted = false
                    rotationStep = 0
                }
            }
        }
    }
}

struct SpinnerItem : View{
    
    let rotation: Double
    let isOffseted: Bool
    let offset: CGFloat
    let isAnimated: Bool
        
    var body: some View {
        let size = CGFloat(!isAnimated ? 100 : 10)
        
        Circle()
            .frame(width: size, height: size)
            .foregroundColor(!isAnimated || !isOffseted ? .red : .green)
            .offset(!isAnimated
                    ? .init(width: 0, height: 0)
                    : .init(width: offset, height: offset)
            )
            .rotationEffect(.init(degrees: rotation))
            .animation(.easeInOut(duration: 1.2), value: isOffseted)
            .animation(.easeInOut, value: isAnimated)
    }
}

struct MultipleAnimationView_Previews: PreviewProvider {
    static var previews: some View {
        MultipleAnimationView()
    }
}
