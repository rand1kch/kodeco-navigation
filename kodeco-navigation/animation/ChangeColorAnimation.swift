//
//  ChangeColorAnimation.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 17.12.2022.
//

import SwiftUI

struct ChangeColorAnimation: View {
    
    @State private var circleColor: Color = .green
    
    var body: some View {
        Button(action: {
            if (circleColor == .green) {
                circleColor = .blue
            }
            else  {
                circleColor = .green
            }
        }, label: {
            Circle()
                .frame(width: 100, height: 100, alignment: .center)
                .foregroundColor(circleColor)
                .animation(.default, value: circleColor)
        }).padding()
    }
}

struct ChangeColorAnimation_Previews: PreviewProvider {
    static var previews: some View {
        VStack{
            ChangeColorAnimation()
        }
    }
}
