//
//  ContentView.swift
//  kodeco-navigation
//
//  Created by Даниил Тимонин on 16.12.2022.
//

import SwiftUI

struct ContentView: View {
    
    @State var userList = users
    
    var body: some View {
        NavigationView{
            VStack(alignment: .leading) {
                HStack {
                    Spacer()
                }
                NavigationLink(destination: {
                    DetailView(user: users[0])
                }, label: {
                    Text("Detail view")
                        .padding()
                })
                NavigationLink(destination: {
                    AnimationView()
                }, label: {
                    Text("Animation")
                        .padding()
                })
                Spacer()
            }
            .navigationBarTitle(Text("Users"))
        }
    }
    
    struct ContentView_Previews: PreviewProvider {
        static var previews: some View {
            ContentView()
        }
    }
}
